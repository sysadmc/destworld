<?
$title='Destroy World - [������]';
include("inc/html_header.php");

echo"<body bgcolor=EBEDEC>";
?>

<center>
<table width=85%>

<tr><td align=center><h3>������������ �������� ������-���� �Destroy World�</h3></td></tr>

<tr>
<td>

<b><u>I. ������ ��������������</u></b>
<br><b>1.1.</b> ����*, ����* � ���� - �������� �� 15 ���
<br><b>1.2.</b> ���������� ������������� �����* - �������� �� 15 ���
<br><b>1.3.</b> �������� � ��������������� �����* (�� ��������� �������� ����**) - �������� �� 15 ���.
<br><b>1.4.</b> �������, ��������� � DW(DestroyWorld), �� ��������� �������� ����** - �������� �� 15 ���. ���������� - ����������� ������ DW.
<br><b>1.5.</b> ������ ���������� ��������� ��������� � �������� ����** (���� ������ � 1 ���)* - �������� 15 ���. ���������� - ����������� ������ DW.
<br><b>1.6.</b> ������� � ����, � �� ������ DW, �� �����, �������� �� �������� - �������� �� 15 ���.
<br><b>1.7.</b> ������ � ���������� ��������� ������������ ���������� ������ ���������� � �������������, ������� ��������� ���������* - �������� 1 ��� � �����.

<br><br><br>

<b><u>II. �������������� ������� �������</u></b>
<br><b>2.1. ����������� ������� ������� ������� �������:</b>
<br><i>2.1.1.</i> ��� ������������ ����������� ���������, �������� ������������� ����������� - �������� ������ 30 �����*;
<br><i>2.1.2.</i>  � ��������� ���������� ������ ����������, ����������� ������� DW - �������� ������ 1-2 ����*;
<br><i>2.1.3.</i> � ��������� ������������� DW � ���������� ��������� - �������� ������ 6 �����.
<br><b>2.2. ��������� ������������ ���������� � ���, ������� ���������������, ����������, ������� ���������:</b>
<br><i>2.2.1.</i> � ��������� ������������� (-��) ���������� DW - �������� ������ 2 ����;
<br><i>2.2.2.</i> �����������, �� ������ ���� - �������� ������ 1 ���;
<br><i>2.2.3.</i> � ��������� ����������, ����������� ������� DW - �������� ������ 4 ����;
<br><i>2.2.4.</i> � ��������� ������������� DW � ���������� ��������� - �������� ������ 12 ���;
<br><i>2.2.5.</i> � ������ ������� - �������� ������ 1 ���
<br><b>2.3. ��������������� ������ �� �������, �� ��������� � DW:</b>
<br><i>2.3.1.</i> �� ������ ������-����* - �������� ������ 3 ����;
<br><i>2.3.2.</i> �� �����, ����������������� ���������� - �������� ������ 1 �����;
<br><i>2.3.3.</i> �� �����, ���������������� ��������� - �������� ������ 1 �����; 
<br><i>2.3.4.</i> ���������� ������������� �������, �� ����������� ��� �������� ������ 2.3.3.  - �������� ������ 1-2 �����.
<br><i>2.3.5.</i> ������ �� ������ �������, ����� ���������� ����������� ������� - �������� ������ 1 ���.
<br><b>2.4. �����������������:</b>
<br><i>2.4.1.</i> ������� � ������ DW � ��� ��������� (��������� ���������, ������, �����) - ������ ������ 1 �����;
<br><i>2.4.2.</i> ������� � �������������, �������� ����������� - ������ ������ 1-2 �����
<br><i>2.4.3.</i> ����������, ������������� �������, � ������ ���������� �������, � ��� �� ����������� ������������� - ������ ������ 1-2 �����.
<br><b>2.5. �������:</b>
<br><i>2.5.1.</i> ������� �� ��������� DW - ������ ������ 1 �����;
<br><i>2.5.2.</i> ������� �� ���������� ������ ����������, ����������� ������� DW - ������ ������ 3 �����
<br><i>2.5.3.</i> ������� �� ������������� DW � ���������� ���������  - ������ ������ 7 �����
<br><i>2.5.4.</i> � ��������� �������, ���������� ��������������� DW � 1 �����.
<br><b>2.6. ������ �������� � �����:</b>
<br><i>2.6.1.</i> � ���������� DW - �������� ���������� ������ 15 �����
<br><i>2.6.2.</i> � ���������� DW � ����������� ������� �� - �������� ���������� ������ 20 �����
<br><i>2.6.3.</i> � ������������ DW � ���������� ��������� - �������� ���������� ������ 1 ������
<br><b>2.7. ����� �������</b>
<br><i>2.7.1.</i> ����� � ������: ������ 7 �����, � ����������� ��������� ������� � ������� ����������. ��� ���������� - ���������� ��������� ���������.

<br><br><br><br>



<b><u>III. ������ ��������������</u></b>
<br><b>3.1.</b> ������� ����������� ������, ���������� �� ������ - ����������� ����������.
<br><b>3.2. �����-������� � ����� �����:</b>
<br><i>3.2.1.</i> ���������� ������ DW, � ����� ������� � �������, ������ ��������� - ������ ������ 11 �������;
<br><i>3.2.2.</i> ���������� �� ������ ��� - ����������� ����������
<br><b>3.3.</b> ������ ���������� DW, ���� DW, ������ DW - ����������� ����������.
<br><b>3.4.</b> ����� �������� ��������� ����������� ������. ����������� ����������, ��� ����������� � ��������. ������ ����� ����������� ������ ����� ������� ������������� ��� ���������� ���������  �� ����������� ����. ������ ������������� � ��������� ��������� ���������� ����� ��������, ���������� ���������� ������ �����. 

<br><br><br><br>

<b><u>����������:</u></b>
<br>* - ������������ �������������� ����� ����������
<br>** - �������� ����: ���������� �������� ����.
<br>� ������ ���������� ������� ��������� �������� (�������) �� ��������� ����� �� ��������� ����� �������������� - ���� ��������� �����������.
<br>� ������� ���������� ������� ��������� ����� 2-� ��� � ������� ����� (��������� ��������) ��� ������� �������, �� ��������� ����� �������������� - ���� ���������� ������������� � 3 ���� � ����� ������ - ���� ��������� ������������� � 2 ����.

<br><br>�������� ���������� ����� ���� ���������� � 2-� ������� ���� ���������� ���������.

<br><br><b>����</b> - ���������� ����� �����, ����� ����� 3-� ��� � 1 ���;
<br><b>����</b> - �������� � ��� �������� ��� �������������� ������ ����, ���� ����� 2-� ��� � 1 ���.

<br><br><b><u>P.S.</u></b>: �������� ���������� � ����������.


</td>
</tr>
</table>
</center>